#!/usr/bin/env  python
# Sample Settings file
# More Information: http://readthedocs.org/docs/pelican/en/2.7.2/settings.html

# Basic Settings
SITENAME            = 'My Blog (powered by Pelican)'

DEFAULT_DATE_FORMAT = '%Y-%m-%d %H:%M:%S'

OUTPUT_PATH         = 'output/'
PATH                = 'entries/'
STATIC_PATHS        = ['assets']

# Pagination
DEFAULT_PAGINATION  = 5

# Tag Cloud
TAG_CLOUD_SETPS     = 5
TAG_CLOUD_MAX_ITEMS = 100

# Translations
DEFAULT_LANG        = 'en'

# Theming
THEME               = 'simple'
THEME_STATIC_PATHS  = ['static']
