Title: Welcome to Pelican on fluxflex.com
Date: 2011-10-19
Slug: first-post

# What is `Pelican` ?

QUOTE: [Pelican — Pelican v2 documentation](http://readthedocs.org/docs/pelican/en/2.7.2/)

> Pelican is a simple weblog generator, writen in python.
> 
> * Write your weblog entries directly with your editor of choice (vim!) and directly in restructured text, or markdown.
> * A simple cli-tool to (re)generate the weblog.
> * Easy to interface with DVCSes and web hooks
> * Completely static output, so easy to host anywhere !

# What is this package ?

This package is `Pelican` toolkit for generates contents on [fluxflex.com](https://www.fluxflex.com/)

# How to use

    $ git clone git://github.com/nyarla/fluxflex-pelican.git my-blog
    $ cd ./my-blog
    $ emacs -nw settings.py # Edit settings
    $ emacs -nw entries/first-post.md # Create your entry
    $ git add . && git commit -m {YOUR-COMMIT-MESSAGE}
    $ git remote add fluxflex {GIT-REPOSITORY-ON-FLUXFLEX-COM}
    $ git push fluxflex master # push && generate your blog

# Package Author

Naoki Okamura (Nyarla) *nyarla[ at ]thotep.net* (I'm Japanese)

# License

This package is under public domain.
